import 'package:flutter/material.dart';
import 'package:kjn_outlet/views/login.dart';

class Beranda extends StatefulWidget {
  @override
  _BerandaState createState() => _BerandaState();
}

class _BerandaState extends State<Beranda> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Beranda"),
      ),
      body: Center(
        child: Material(
          borderRadius: BorderRadius.circular(10.0),
          color: Colors.deepOrange,
          child: MaterialButton(
              minWidth: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(10),
              onPressed: () {
                setState(() {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(builder: (BuildContext context) => Login()),
                  );
                });
              },
              child: Text("Login", textAlign: TextAlign.center)),
        ),
      ),
    );
  }
}
