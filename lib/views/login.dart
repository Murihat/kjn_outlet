import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:kjn_outlet/main.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);

  final GlobalKey<FormBuilderState> _formKey = GlobalKey<FormBuilderState>();

  final _email = TextEditingController();
  final _password = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void saveData() {
    final form = _formKey.currentState;
    if (form.saveAndValidate()) {
      Map contact = {
        'email': _email.text,
        'password': _password.text,
      };
      print(contact);
    } else {
      print('form is invalid');
    }
  }

  @override
  Widget build(BuildContext context) {
    final emailField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Email",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );

    final passwordField = TextField(
      obscureText: true,
      style: style,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "Password",
          border:
              OutlineInputBorder(borderRadius: BorderRadius.circular(10.0))),
    );

    final lupaPassword = Container(
      width: MediaQuery.of(context).size.width,
      child: Text("Lupa Password ?",
          style: TextStyle(color: Colors.deepOrange),
          textAlign: TextAlign.right),
    );

    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(10.0),
      color: Colors.deepOrange,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(8),
        onPressed: () {
          setState(() {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (BuildContext context) => RootPage()),
            );
          });
        },
        child: Text("Masuk",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    final orRegistrasi = Row(
      children: <Widget>[
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 10.0, right: 20.0),
              child: Divider(
                color: Colors.grey,
                height: 36,
              )),
        ),
        Text("Atau"),
        Expanded(
          child: new Container(
              margin: const EdgeInsets.only(left: 20.0, right: 10.0),
              child: Divider(
                color: Colors.grey,
                height: 36,
              )),
        ),
      ],
    );

    final btnRegistrasi = Material(
      // elevation: 5.0,
      borderRadius: BorderRadius.circular(10.0),
      // color: Colors.deepOrange,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(8),
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0),
            side: BorderSide(color: Colors.deepOrange)),
        onPressed: () {},
        child: Text("Daftar",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.grey[600], fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
        body: SingleChildScrollView(
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
              color: Colors.deepOrange,
              image: DecorationImage(
                  image: ExactAssetImage("assets/bg-full-new.png"),
                  fit: BoxFit.cover)),
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: SizedBox(
                    height: 150.0,
                    child: Image.asset(
                      "assets/kjn-logo-white.png",
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 15.0),
                      emailField,
                      SizedBox(height: 25.0),
                      passwordField,
                      SizedBox(
                        height: 15.0,
                      ),
                      lupaPassword,
                      SizedBox(
                        height: 20.0,
                      ),
                      loginButon,
                      SizedBox(
                        height: 15.0,
                      ),
                      // Divider(color: Colors.deepOrange, thickness: ,),
                      orRegistrasi,
                      SizedBox(
                        height: 15.0,
                      ),
                      btnRegistrasi
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    ));
  }
}
