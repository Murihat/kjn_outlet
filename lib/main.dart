import 'package:flutter/material.dart';
import 'package:kjn_outlet/views/akun.dart';
import 'package:kjn_outlet/views/beranda.dart';
import 'package:kjn_outlet/views/login.dart';
import 'package:kjn_outlet/views/notifikasi.dart';
import 'package:kjn_outlet/views/transaksi.dart';
// import 'package:kjn_outlet/util/color_utils.dart' show HexColor;


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'KJN OUTLET',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Login(),
    );
  }
}

class RootPage extends StatefulWidget {
  @override
  _RootPageState createState() => _RootPageState();
}

class _RootPageState extends State<RootPage> {
  int _selectedIndex = 0;

  final _layoutPage = [Beranda(), Transaksi(), Notifikasi(), Akun()];

  void _onTabItem(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _layoutPage.elementAt(_selectedIndex),
      bottomNavigationBar: SizedBox(
        height: 55.0,
        width: MediaQuery.of(context).size.width,
        child: BottomNavigationBar(
          backgroundColor: Colors.deepOrange,
          selectedItemColor: Colors.white,
          unselectedItemColor: Colors.grey[400],
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.home), title: Text('Beranda')),
            BottomNavigationBarItem(
                icon: Icon(Icons.map), title: Text('Transaksi')),
            BottomNavigationBarItem(
                icon: Icon(Icons.message), title: Text('Notifikasi')),
            BottomNavigationBarItem(
                icon: Icon(Icons.account_circle), title: Text('Akun')),
          ],
          type: BottomNavigationBarType.fixed,
          currentIndex: _selectedIndex,
          onTap: _onTabItem,
        ),
      ),
    );
  }
}
